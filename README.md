# AlloCinéClone - ChameleonMovies

![Rango alt](https://upload.wikimedia.org/wikipedia/en/thumb/6/6d/Rango2011Poster.jpg/220px-Rango2011Poster.jpg)


L'objectif est de créer un site internet de recherche d’informations sur des films. Grâce à ce site, on va pouvoir recherche n’importe quel film et connaître toutes les infos liées au film (titre, durée, réalisateur, etc ..).

## Pour commencer

Pour toutes les informations, je vais utiliser l’API de The Movie DB : https://developers.themoviedb.org/3/getting-started/introduction.

Le site doit proposer un champ de recherche qui permet de taper le titre d’un film et de faire une recherche sur la base de données. La recherche doit donner une liste des meilleures correspondance avec ce que l’utilisateur a tapé. Cette liste devra être sur plusieurs pages.

Chaque élément des résultats de recherche doit être cliquable et amène vers la page du film. Dans cette page, on affiche toutes les informations utiles pour le film. Déterminer les informations les plus pertinentes.

Aucune maquette graphique n'a été définie pour cette page, j'ai carte blanche (travail individuel).

Mettre l’accent sur l’UI/UX, l’accessibilité et le SEO du site.

Il s’agit d’un produit minimum viable. 


### Pré-requis

- Points obligatoires
- Le site sera réalisé en HTML5, CSS3 et JS ES6+
- Le site doit être responsive, respecter les règles d’accessibilité et être optimisé pour le SEO
- Utiliser SASS pour la création du CSS
- Utiliser un framework CSS
- Utiliser npm pour la gestion des dépendances 
- Utiliser ESLint et StyleLint pour le style du code (notamment ES6)
- Points facultatifs
- Utiliser n’importe quelles librairies JS ou CSS, tant qu’elles sont installées avec npm


### Livrables


Un dépot Gitlab contenant le code source (HTML, SASS, CSS, Images, ...), ainsi que le schéma de la maquette graphique. Ce schéma doit être au format image (JPG, PNG) et peut être fait soit sur un outil numérique ou alors juste en mode papier/crayon. 


## Fabriqué avec

* [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript) - Langage de script léger, orienté objet
* [Bootstrap](https://getbootstrap.com/) - Framework CSS (front-end)
* [VisualStudioCode](https://code.visualstudio.com/) - Editeur de textes


## Auteur

* **Christophe Grava** 
