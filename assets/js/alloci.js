import { API_KEY } from '../../config.js';

const url = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}`;

const IMG_URL = 'https://image.tmdb.org/t/p/w500';

const imgNone = './img/chameleon.jpg'; // image pour remplacer la valeur null de l'affiche du film

const emptyImage = document.getElementById('emptyImage'); // partie html réservée à laffichage de l'image dans la modale

const buttonElement = document.getElementById('rechercher'); // bouton pour lancer la recherche d'un film

const input = document.getElementById('inputValue'); // champ de recherche du film

const movieSearchable = document.getElementById('movies-searchable'); // partie html pour l'affichage des films

const moviesContainer = document.getElementById('movies-container');  // partie html pour l'affichage des films à venir / populaires / très bien notés

const sr = ScrollReveal();

sr.reveal('body', {
  delay: 500,
});

function generateUrl(path) {
  const url = `https://api.themoviedb.org/3${path}?api_key=${API_KEY}&language=fr`;
  return url;
}

function requestMovies(url, onComplete, onError) {
  fetch(url)
    .then((res) => res.json()) // résultat au format json
    .then(onComplete)
    .catch(onError);
}

function searchMovie(value) {
  const path = '/search/movie';
  const url = `${generateUrl(path)}&query=${value}`;

  requestMovies(url, renderSearchMovies, handleError);
}

function handleError(error) {
  console.log('Error:', error);
}

// fonction qui permet d'afficher l'image du film

function movieSection(movies) {
  return movies.map((movie) => {
    if (movie.poster_path) {
      return `<img class="imgMovies"
            src=${IMG_URL + movie.poster_path}
            data-movie-id=${movie.id}
            alt="Affiche du film"
            itemprop="image"
            />`;
    }

    if (movie.poster_path === null) {
      return `<img class="imgMovies"    
            src=${imgNone} 
            data-movie-id=${movie.id}
            alt="Image remplaçant l'affiche manquante du film"
            itemprop="image"
            />`;
    }
  });
}

// fonction qui prend les données films dans la fonction globale

function createMovieContainer(movies, title = '') {
  const movieElement = document.createElement('div');
  movieElement.setAttribute('class', 'movie');
  const movieTemplate = `
    <h2>${title}</h2>
    <div class="section1" itemscope itemtype="https://schema.org/Movie">   
    ${movieSection(movies)}
    </div>
    `;

  // data-spy="scroll" data-target=".imgMovies" data-offset="0" class="scrollspy-example"

  movieElement.innerHTML = movieTemplate;
  return movieElement;
}

function renderSearchMovies(data) {
  // data.results []
  movieSearchable.innerHTML = ''; // permet de reset le résultat de la recherche dans movieSearchable côté HTML
  const movies = data.results;
  const movieBlock = createMovieContainer(movies);
  movieSearchable.appendChild(movieBlock);
  console.log('Data:', data);
}

function renderMovies(data) {
  // data.results []
  const movies = data.results;
  const movieBlock = createMovieContainer(movies, this.title);
  moviesContainer.appendChild(movieBlock);
  console.log('Data:', data);
}

// fonction globale qui permet de récupérer toutes les données de TMDB

buttonElement.onclick = function (event) {
  event.preventDefault(); // évite le refresh de la page à partir du formulaire
  const { value } = inputValue; // permet de récupérer la valeur de l'input
  searchMovie(value);

  input.value = ''; // permet de reset le champ de recherche du film
  console.log('Value:', value);
};

// fonction pour permettre l'affichage des données d'un film après avoir cliqué sur l'image du film

document.onclick = function (event) {
  const { target } = event;

  if (target.tagName.toLowerCase() === 'img') {
    const { movieId } = target.dataset;
    console.log('Movie ID:', movieId);
    const path1 = `/movie/${movieId}`;
    const url1 = generateUrl(path1);

    $('#exampleModal').modal('toggle');

    fetch(url1)
      .then((res) => res.json()) // résultat au format json
      .then((data) => {
        console.log(data);

        if (data.poster_path === null) {
          emptyImage.classList.add('imgMagie');
          emptyImage.setAttribute('src', `${imgNone}`);
        } else if (data.poster_path != null) {
          emptyImage.setAttribute('src', `https://image.tmdb.org/t/p/w200${data.poster_path}`);
          emptyImage.setAttribute('itemprop', 'image'); // SEO
          emptyImage.setAttribute('alt', 'Affiche du film'); // SEO
        }

        document.getElementById('title').innerHTML = 'Titre du film : ' + `${data.title}`;
        document.getElementById('title').setAttribute('itemprop', 'name'); // SEO

        document.getElementById('genre').innerHTML = 'Genre du film : ' + `${data.genres[0].name}`;
        document.getElementById('genre').setAttribute('itemprop', 'genre'); // SEO

        document.getElementById('date').innerHTML = 'Date de sortie : ' + `${data.release_date}`;
        document.getElementById('date').setAttribute('itemprop', 'datePublished'); // SEO

        document.getElementById('overview').innerHTML = 'Pitch du film : ' + `${data.overview}`;
        document.getElementById('overview').setAttribute('itemprop', 'description'); // SEO

        document.querySelector('#example2').setAttribute('style', 'background-color: orange;');
        document.querySelector('#example2').setAttribute('itemscope', ''); // SEO
        document.querySelector('#example2').setAttribute('itemtype', 'https://schema.org/Movie'); // SEO
      })

      .catch((error) => {
        console.log('Error:', error);
      });

    const path2 = `/movie/${movieId}/credits`;
    const url2 = generateUrl(path2);

    fetch(url2)
      .then((res) => res.json()) // résultat au format json
      .then((tada) => {
        console.log(tada);

        const actors = tada.cast;

        for (let i = 0; i < 5; i++) {
          const section = document.getElementById('section');
          const myArticle = document.createElement('article');
          const myH3 = document.createElement('h3');
          const myImg = document.createElement('img');

          myH3.textContent = actors[i].name;
          myH3.setAttribute('itemprop', 'name'); // SEO
          myH3.setAttribute('itemscope', ''); // SEO
          myH3.setAttribute('itemtype', 'https://schema.org/Person'); // SEO

          if (actors[i].profile_path === null) {
            myImg.classList.add('imgMagie');
            myImg.setAttribute('src', `${imgNone}`);
          } else if (actors[i].profile_path != null) {
            myImg.setAttribute('src', `https://image.tmdb.org/t/p/w200${actors[i].profile_path}`);
            myImg.setAttribute('itemprop', 'image'); // SEO
            myImg.setAttribute('alt', 'Photo acteur'); // SEO
          }

          myArticle.appendChild(myH3);
          myArticle.appendChild(myImg);
          section.appendChild(myArticle);
        }

        myButton.addEventListener('click', () => {
          section.innerHTML = '';
        });
      })

      .catch((error) => {
        console.log('Error:', error);
      });

    /// ///////////////////////////////////////////////////////////////////////////
    /// //////////////////             PAGINATION  NON ABOUTIE            /////////
    /// ///////////////////////////////////////////////////////////////////////////

    // const path3 = `/movie/${movieId}/lists`;
    // const url3 = generateUrl(path3);

    // fetch(url3)
    //     .then((res) => res.json())  //résultat au format json
    //     .then((lata) => {

    //         console.log(lata.page.value)

    // let moviePages = lata['page'];

    // for (let i = 0; i < moviePages.length; i++) {

    // //             <ul class="pagination">
    // //   <li class="page-item"><a class="page-link" href="#">Previous</a></li>
    // //   <li class="page-item"><a class="page-link" href="#  ">1  numéro de la page   </a></li>

    // //   <li class="page-item"><a class="page-link" href="#">Next</a></li>
    // // </ul>

    //         const moviesPagination = document.getElementById('moviesPagination');
    //         let myUl = document.createElement('ul');
    //         let myLi = document.createElement('li');
    //         let myLink = document.createElement('a');

    //         myLink.textContent = moviePages[i];
    //         myLink.classList.add('page-link');
    //         myLi.classList.add('page-item');
    //         myUl.classList.add('pagination');

    //     // let section = document.getElementById('section');
    //     // let myArticle = document.createElement('article');
    //     // let myH3 = document.createElement('h3');
    //     // let myImg = document.createElement('img');

    //     // myH3.textContent = moviePages[i].name;
    //     // myH3.setAttribute('itemprop', 'name'); //SEO
    //     // myH3.setAttribute('itemscope', ''); //SEO
    //     // myH3.setAttribute('itemtype', 'https://schema.org/Person'); //SEO

    //         myLi.appendChild(myLink);
    //         myUl.appendChild(myLi);
    //        moviesPagination.appendChild(myUl);

    //     }

    // })

    // .catch((error) => {
    //     console.log('Error:', error);
    // })
  }
};

// films les mieux notés

function getTopRatedMovies() {
  const path = '/movie/top_rated';
  const url = generateUrl(path);
  const render = renderMovies.bind({ title: 'Films les mieux notés' });
  requestMovies(url, render, handleError);
}

// films les plus populaires

function getPopularMovies() {
  const path = '/movie/popular';
  const url = generateUrl(path);
  const render = renderMovies.bind({ title: 'Films les plus populaires' });
  requestMovies(url, render, handleError);
}

// films à venir

function getUpcomingMovies() {
  const path = '/movie/upcoming';
  const url = generateUrl(path);
  const render = renderMovies.bind({ title: 'Films à venir' });
  requestMovies(url, render, handleError);
}

getTopRatedMovies();
getPopularMovies();
getUpcomingMovies();



// ANIMATION CHANGEMENT DE COULEUR DU BODY 

const bodyColor = document.querySelector('body')

window.addEventListener('scroll', () => {
    console.log('Scroll !')
    
    if (window.scrollY > 400) {
        bodyColor.classList.add('scrolly')

    }

    else {
        bodyColor.classList.remove('scrolly')
    
    }

})

